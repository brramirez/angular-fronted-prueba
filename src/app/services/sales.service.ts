import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SaleProduct } from '../Models/saleProducts';
import { Sale } from '../Models/sales';

@Injectable({
  providedIn: 'root'
})
export class SalesService {

  private base_url = "https://localhost:44392/api"; // URL to web api
  constructor(private http: HttpClient ) {

}
public getSales(){
  return this.http.get(this.base_url+'/sale');
}
public getSale(id: number){
  return this.http.get(this.base_url+'/sale/'+id);
}

public setSale(sale:Sale){
  console.log(sale)
  return this.http.post<Sale>(this.base_url+'/sale',sale);
}
public setSaleProduct(saleProduct:SaleProduct){
  console.log(saleProduct)
  return this.http.post<SaleProduct>(this.base_url+'/sale/items',saleProduct);
}



}
