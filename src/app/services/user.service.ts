import { Injectable } from '@angular/core';
import { User } from '../Models/users';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private user: User ;
  private base_url = "https://localhost:44392/api"; // URL to web api
  constructor(private http: HttpClient ) {
}

public getUsers(){
  return this.http.get(this.base_url+'/user');
}
public getUser(id : number){
  return this.http.get(this.base_url+'/user/'+id);
}

public createUser(user:User){
  return this.http.post<User>(this.base_url+'/user', user);
}

public editUser(user:User){
  return this.http.post<User>(this.base_url+'/user/'+user.userId+'/update', user);
}

public deleteUser(id:number){
  return this.http.post<User>(this.base_url+'/user/'+id+'/delete', new User);
}

}
