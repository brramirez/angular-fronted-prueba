import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from '../Models/products';
import { User } from '../Models/users';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  private products: Product ;
  private base_url = "https://localhost:44392/api"; // URL to web api
  constructor(private http: HttpClient ) {
}

public getProducts(){
  return this.http.get(this.base_url+'/product');
}
public getProduct(id : number){
  return this.http.get(this.base_url+'/product/'+id);
}

public createProduct(product:Product){
  return this.http.post<User>(this.base_url+'/product', product);
}

public editProduct(product:Product){
  return this.http.post<User>(this.base_url+'/product/'+product.productId+'/update', product);
}

public deleteProducts(id:number){
  return this.http.post<User>(this.base_url+'/product/'+id+'/delete', new Product);
}
}
