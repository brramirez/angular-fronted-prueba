import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {

  menu:any[]= [
    {
    titulo:'Usuarios',
    icono: 'mdi mdi-account',
    submenu:[
      {titulo:'Gestión de usuarios', url:'/users'
      }
    ]


    },
    {
      titulo:'Productos',
      icono: 'mdi mdi-clipboard',
      submenu:[
        {titulo:'Gestión de productos',
         url:'/products'
        },
      ]


      },
      {
        titulo:'Ventas',
        icono: 'mdi mdi-cart',
        submenu:[
          {titulo:'Gestión de ventas',
           url:'/store'
          }
        ]

        }
  ]

  constructor() { }
}


