export class User {
  public userId: number;
  public userName: string;
  public userLastName: string;
  public userNumIdent: number;
  public userBirthDate: Date;
  public userPhone: number;
}
