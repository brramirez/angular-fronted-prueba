import {User} from "./users";
import {SaleProduct} from "./saleProducts";

export class Sale {
  saleId: number;
  saleUserId: number;
  saleAmount: number;
  saleDateSale: Date;
  saleUser: User;
  saleProducts: SaleProduct[];
}
