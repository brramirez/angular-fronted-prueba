import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../services/user.service";
import {SalesService} from "../../../services/sales.service";
import {Sale} from "../../../Models/sales";
import {User} from "../../../Models/users";

@Component({
  selector: 'app-show-sale',
  templateUrl: './show-sale.component.html',
  styles: []
})
export class ShowSaleComponent implements OnInit {

  sale: Sale = new Sale();
  id: number = 0;

  constructor(private router: Router, private saleService: SalesService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe((params: any) => {
      this.id = params['sale_id'];
    });
  }

  ngOnInit(): void {
    this.saleService.getSale(this.id).subscribe((response) => {
      this.sale = response as Sale;
      console.log(this.sale);
    });
  }


}
