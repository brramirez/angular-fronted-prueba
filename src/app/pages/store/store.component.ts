import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Product} from 'src/app/Models/products';
import {Sale} from 'src/app/Models/sales';
import {User} from 'src/app/Models/users';
import {SalesService} from 'src/app/services/sales.service';

@Component({
  selector: 'app-store',
  templateUrl: './store.component.html',
  styles: []
})
export class StoreComponent implements OnInit {

  sales: Sale[] = [];

  constructor(private saleService: SalesService, private router: Router) {
  }


  ngOnInit(): void {
    this.saleService.getSales().subscribe((dataSales) => {
      this.sales = dataSales as Sale[];
    });
  }

}
