import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Product} from 'src/app/Models/products';
import {SaleProduct} from 'src/app/Models/saleProducts';
import {Sale} from 'src/app/Models/sales';
import {User} from 'src/app/Models/users';
import {ProductsService} from 'src/app/services/products.service';
import {SalesService} from 'src/app/services/sales.service';
import {UserService} from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-sale',
  templateUrl: './new-sale.component.html',

})
export class NewSaleComponent implements OnInit {
  users: User[] = [];
  products: Product[] = [];
  user: User = new User();
  sale: Sale = new Sale();
  saleProduct: SaleProduct = new SaleProduct();
  user_id: number;
  date_sale: Date;
  amount: number;


  saleForm = new FormGroup({
    productId: new FormControl('', Validators.required),
    userId: new FormControl('', Validators.required),
    quantity: new FormControl('', Validators.required),
    dateSale: new FormControl('', Validators.required),
  });

  constructor(private userService: UserService, private productService: ProductsService, private saleService: SalesService, private router: Router) {
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe((dataUsers) => {
      this.users = dataUsers as User[];
    });

    this.productService.getProducts().subscribe((dataProducts) => {
      this.products = dataProducts as Product[];
    });
  }

  newSale() {
    if (this.saleForm.valid) {
      let product = this.products.find(item => item.productId == this.saleProduct.saleProductsProductId);

      this.saleProduct.saleProductsUnitPrice = product.productPrice;
      this.amount = product.productPrice * this.saleProduct.saleProductsQty;
      this.sale.saleAmount = this.amount;

      this.saleService.setSale(this.sale).subscribe(response => {
        this.saleProduct.saleProductsSaleId = response.saleId;
        this.saleProduct.saleProductsProductId = product.productId;
        this.saleService.setSaleProduct(this.saleProduct).subscribe((response) => {
          Swal.fire({
            icon: 'success',
            title: 'Venta guardada correctamente',
            showConfirmButton: false,
            timer: 1500
          });
          this.router.navigateByUrl('/store');
        });
      });
    }else
    alert("Asegúrese de completar el formulario");
  }


}
