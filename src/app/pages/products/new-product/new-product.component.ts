import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Product} from 'src/app/Models/products';

import {ProductsService} from 'src/app/services/products.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
})
export class NewProductComponent {

  product: Product = new Product();

  productForm = new FormGroup({
    product_name: new FormControl('', Validators.required),
    product_description: new FormControl('', Validators.required),
    product_stock: new FormControl('', Validators.required),
    product_price: new FormControl('', Validators.required),

  });

  constructor(private productService: ProductsService, private router: Router) {
  }

  newProduct() {
    if (this.productForm.valid)
      this.productService.createProduct(this.product).subscribe(response => {
        Swal.fire({
          icon: 'success',
          title: 'Producto agregado correctamente',
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigateByUrl('/products');
      });
    else
      alert("Asegúrese de completar el formulario");
  }
}
