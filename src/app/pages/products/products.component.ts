import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Product} from 'src/app/Models/products';
import {ProductsService} from 'src/app/services/products.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styles: []
})
export class ProductsComponent implements OnInit {

  products: Product[] = [];

  constructor(private productService: ProductsService, private router: Router) {
  }

  ngOnInit(): void {
    this.productService.getProducts().subscribe((dataProducts) => {
      this.products = dataProducts as Product[];
    });
  }

  delete(id: number) {
    this.productService.deleteProducts(id).subscribe((response) => {
      Swal.fire({
        icon: 'success',
        title: 'Producto eliminado correctamente',
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigateByUrl('/products');
    }, (error) => {
      Swal.fire({
        icon: 'error',
        title: "Error",
        text: 'El Producto no se pudo eliminar',
      });
    });
  }

}
