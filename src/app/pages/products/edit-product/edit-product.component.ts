import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {Product} from 'src/app/Models/products';
import {User} from 'src/app/Models/users';
import {ProductsService} from 'src/app/services/products.service';
import {UserService} from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
})
export class EditProductComponent implements OnInit {
  product: Product = new Product();

  id: number = 0;

  constructor(private router: Router, private productService: ProductsService, private activatedRoute: ActivatedRoute) {
    this.activatedRoute.params.subscribe((params: any) => {
      this.id = params['product_id'];
    });
  }

  ngOnInit(): void {
    this.productService.getProduct(this.id).subscribe((response) => {
      this.product = response as Product;
    });
  }

    edit_productForm = new FormGroup({
    product_name: new FormControl('', Validators.required),
    product_description: new FormControl('', Validators.required),
    product_stock: new FormControl('', Validators.required),
    product_price: new FormControl('', Validators.required),
  });

  editProduct() {
    if (this.edit_productForm.valid)
      this.productService.editProduct(this.product).subscribe(response => {

        Swal.fire({
          icon: 'success',
          title: 'Producto editado correctamente',
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigateByUrl('/products');
      });
    else
      alert("Asegurese de completar el formulario");
  }
}
