import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from 'src/app/Models/users';
import { ActivatedRoute, Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-edit-user',
  templateUrl: './edit-user.component.html',
})
export class EditUserComponent  {


  user:User = new User();
  id: number = 0;

  constructor(private router: Router, private userService : UserService, private activatedRoute: ActivatedRoute) {
   this.activatedRoute.params.subscribe((params: any) => {
     this.id = params['user_id'];
   });
  }

 ngOnInit(): void {
   this.userService.getUser(this.id).subscribe((response) => {
     this.user = response as User;
   });
 }

  edit_userForm = new FormGroup({
    name: new FormControl('',Validators.required),
    last_name: new FormControl('', Validators.required),
    num_ident: new FormControl('', Validators.required),
    birth_date: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
  });

  editUser(){
    if (this.edit_userForm.valid)
    this.userService.editUser(this.user).subscribe( response => {
      Swal.fire({
        icon: 'success',
        title: 'Usuario editado correctamente',
        showConfirmButton: false,
        timer: 1500
      })
      this.router.navigateByUrl('/users');
    });
    else
    alert("Asegurese de completar el formulario");
    }


}
