import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from 'src/app/Models/users';
import {UserService} from 'src/app/services/user.service';
import Swal from 'sweetalert2/dist/sweetalert2.js';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
})
export class NewUserComponent {


  user: User = new User();

  userForm = new FormGroup({
    name: new FormControl('', Validators.required),
    last_name: new FormControl('', Validators.required),
    num_ident: new FormControl('', Validators.required),
    birth_date: new FormControl('', Validators.required),
    phone: new FormControl('', Validators.required),
  });

  constructor(private userService: UserService, private router: Router) {
  }

  newUser() {

    if (this.userForm.valid)
      this.userService.createUser(this.user).subscribe(response => {
        Swal.fire({
          icon: 'success',
          title: 'Usuario agregado correctamente',
          showConfirmButton: false,
          timer: 1500
        });
        this.router.navigateByUrl('/users');
      });
    else
      alert("Asegurese de completar el formulario");
  }


}
