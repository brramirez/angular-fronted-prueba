import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from 'src/app/Models/users';
import {UserService} from 'src/app/services/user.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styles: []
})
export class UsersComponent implements OnInit {

  users: User[] = [];

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit(): void {
    this.userService.getUsers().subscribe((dataUsers) => {
      this.users = dataUsers as User[]
    });
  }

  delete(id: number) {

    this.userService.deleteUser(id).subscribe((response) => {
      Swal.fire({
        icon: 'success',
        title: 'Usuario eliminado correctamente',
        showConfirmButton: false,
        timer: 1500
      });
      this.router.navigateByUrl('/users');
    }, (error) => {
      Swal.fire({
        icon: 'error',
        title: "Error",
        text: 'El Usuario no se pudo eliminar',
      });
    });

  }


}
