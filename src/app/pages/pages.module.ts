import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard/dashboard.component';
import { PagesComponent } from './pages.component';
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../app-routing.module';
import { UsersComponent } from './users/users.component';
import { ProductsComponent } from './products/products.component';
import { StoreComponent } from './store/store.component';
import { NewUserComponent } from './users/new-user/new-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { NewProductComponent } from './products/new-product/new-product.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { FormControl, FormsModule, NgModel, ReactiveFormsModule } from '@angular/forms';
import { NewSaleComponent } from './store/new-sale/new-sale.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from '../services/user.service';
import { EdadPipe } from '../pipes/edad.pipe';
import { ShowSaleComponent } from './store/show-sale/show-sale.component';




@NgModule({
  declarations: [
    DashboardComponent,
    PagesComponent,
    UsersComponent,
    ProductsComponent,
    StoreComponent,
    NewUserComponent,
    EditUserComponent,
    NewProductComponent,
    EditProductComponent,
    NewSaleComponent,
    EdadPipe,
    ShowSaleComponent
  ],
  exports:[
    DashboardComponent,
    PagesComponent,
    UsersComponent,
    ProductsComponent,
    StoreComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  
})
export class PagesModule { }
