import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { PagesComponent } from './pages.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { UsersComponent } from './users/users.component';
import { StoreComponent } from './store/store.component';
import { ProductsComponent } from './products/products.component';
import { NewUserComponent } from './users/new-user/new-user.component';
import { EditUserComponent } from './users/edit-user/edit-user.component';
import { NewProductComponent } from './products/new-product/new-product.component';
import { EditProductComponent } from './products/edit-product/edit-product.component';
import { NewSaleComponent } from './store/new-sale/new-sale.component';
import {ShowSaleComponent} from "./store/show-sale/show-sale.component";


const routes: Routes = [

    {
    path:'dashboard',
    component: PagesComponent,
    children:
    [
      {path:'', component: DashboardComponent}
    ]
     },
     {
         path:'users',
         component:PagesComponent,
         children:[
            {path:'', component: UsersComponent},
            {path:'new_user', component:NewUserComponent},
            {path:'edit_user/:user_id', component:EditUserComponent}
         ]
     },
     {
        path:'products',
        component:PagesComponent,
        children:[
           {path:'', component: ProductsComponent},
           {path:'new_product', component:NewProductComponent},
           {path:'edit_product/:product_id', component:EditProductComponent}

        ]
    },

    {
        path:'store',
        component:PagesComponent,
        children:[
           {path:'', component: StoreComponent},
           {path:'new_sale', component:NewSaleComponent},
           {path:'show_sale/:sale_id', component:ShowSaleComponent},
        ]
    }

    //{ path: 'path/:routeParam', component: MyComponent },
    //{ path: 'staticPath', component: ... },
    //{ path: '**', component: ... },
    //{ path: 'oldPath', redirectTo: '/staticPath' },
    //{ path: ..., component: ..., data: { message: 'Custom' }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PageRoutingModule {}
