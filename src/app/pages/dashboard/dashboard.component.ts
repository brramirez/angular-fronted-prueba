import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Sale} from 'src/app/Models/sales';
import {SalesService} from 'src/app/services/sales.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styles: []
})
export class DashboardComponent implements OnInit {

  sales: Sale[] = [];

  constructor(private saleService: SalesService, private router: Router) {
  }


  ngOnInit(): void {
    this.saleService.getSales().subscribe((dataSales) => {
      this.sales = dataSales as Sale[];
    });
  }

}
